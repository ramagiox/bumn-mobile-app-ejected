import React from "react";
import { StatusBar, Image, MapView, AsyncStorage, Alert, TextInput, ImageBackground, View, NativeModules, ImageStore} from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Footer,
    FooterTab,
    H1,
    Thumbnail,
    Form,
    Item,
    Label,
    Input,
    List,
    ListItem

} from "native-base";

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';
import Spinner from 'react-native-loading-spinner-overlay';



//import { ImagePicker } from 'expo';





export default class UbahProfil extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            dataUser: [],
            userId: "",
            email: "",
            mobilePhone: "",
            image: "",
            visible: false,
            encodedData: ""
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("userid", (error, result) => {
            if (result) {
                console.log("userid : " + result)
                this.state.userId = result;
            }
            this.state.userId = result;
            fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile/" + result, {
                method: "GET",
                headers: {
                    'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
                }
            })
                .then((response) => response.json())
                .then((data) => {

                    this.setState({
                        dataUser: data.data,
                        image: data.data.profilePicture,
                        email: data.data.email,
                        mobilePhone: data.data.mobilePhone
                    });
                    // AsyncStorage.setItem("name",data.data.name);
                    // AsyncStorage.setItem("email",data.data.email);
                    // AsyncStorage.setItem("nik",data.data.nik);
                    // AsyncStorage.setItem("bumnName",data.data.bumnName);
                    // AsyncStorage.setItem("mobilePhone",data.data.mobilePhone);
                    // AsyncStorage.setItem("profilePicture",data.data.profilePicture);

                    console.log(this.state.dataUser)
                })
                .catch((error) => {
                    console.log(error);
                })
        })
    }

    render() {
        let { image } = this.state;

        return (
            <Container style={{
                marginTop: 25,
                backgroundColor: "white"
            }}>
                <Header style={{ justifyContent: "center", alignContent: "center", backgroundColor: "white" }}>

                    <Left>
                        <Icon name="arrow-back" onPress={() => this.props.navigation.navigate("Profil")} />
                    </Left>
                    <Body>
                        <Title style={{ color: "black" }}>Ubah Profil</Title>
                    </Body>
                    <Right />
                </Header>
                <Content >
                    <View style={{ flex: 1 }}>
                        <Spinner visible={this.state.visible} textContent={"Loading..."} textStyle={{ color: '#FFF' }} />
                    </View>
                    <View style={{ alignContent: "center", alignItems: "center", marginTop: "5%" }}>
                        <Thumbnail large source={{ uri: this.state.image }} style={{}} />
                        <Text onPress={this._pickImage} style={{ fontSize: responsiveFontSize(2), fontWeight: "bold", marginTop: "5%", color: "#3f84f4" }}>Ubah Foto</Text>
                    </View>
                    <Input defaultValue={this.state.dataUser.name} disabled style={{ color: "#afafaf", backgroundColor: "#e8e8e8", marginLeft: "10%", marginRight: "10%", marginTop: "5%", borderRadius: 5, paddingLeft: "5%" }} />
                    <Input keyboardType="numeric" onChangeText={this.handleMobilePhone} defaultValue={this.state.dataUser.mobilePhone} placeholder='No. Handphone' style={{ backgroundColor: "#e8e8e8", marginLeft: "10%", marginRight: "10%", marginTop: "1%", borderRadius: 5, paddingLeft: "5%" }} />
                    <Input onChangeText={this.handleEmail} defaultValue={this.state.dataUser.email} placeholder='Email' style={{ backgroundColor: "#e8e8e8", marginLeft: "10%", marginRight: "10%", marginTop: "1%", borderRadius: 5, paddingLeft: "5%" }} />
                    <Input defaultValue={this.state.dataUser.nik} disabled style={{ color: "#afafaf", backgroundColor: "#e8e8e8", marginLeft: "10%", marginRight: "10%", marginTop: "1%", borderRadius: 5, paddingLeft: "5%" }} />
                    <Input defaultValue={this.state.dataUser.bumnName} disabled style={{ color: "#afafaf", backgroundColor: "#e8e8e8", marginLeft: "10%", marginRight: "10%", marginTop: "1%", borderRadius: 5, paddingLeft: "5%" }} />

                    <Button onPress={this.editUser} style={{ borderRadius: 5, backgroundColor: "#0052A8", marginLeft: "15%", width: "70%", marginTop: "5%", flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Text>Simpan</Text>
                    </Button>
                </Content>
            </Container>
        );
    }

    handleEmail = (text) => {
        this.setState({ email: text })
    }
    handleMobilePhone = (text) => {
        this.setState({ mobilePhone: text });

    }

    _pickImage = async () => {
        // let result = await ImagePicker.launchImageLibraryAsync({
        //     allowsEditing: true,
        //     aspect: [4, 3],
        // });

        console.log(result);

        if (!result.cancelled) {
            console.log(result);
            this.setState({ image: result.uri });

        }
    };


    editUser = () => {
        if (this.state.email == "" || this.state.mobilePhone == "") {
            Alert.alert(
                'Pesan',
                'Email/No. Handphone tidak boleh kosong',
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {
                        text: 'OK', onPress: () => {
                        }
                    },
                ],
                { cancelable: false }
            )

        } else {
            this.setState({
                visible: !this.state.visible
            });
            AsyncStorage.getItem("userid", (error, result) => {
                if (result) {
                    console.log("userid : " + result)
                    this.state.userId = result;
                }
                let apiUrl = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:3000/api/V1/profile/' + result;
                let uri = this.state.image;

                let formData = new FormData();
                formData.append('profilePicture', {
                    uri,
                    name: `photo.png`,
                    type: `image/png`,
                });
                formData.append('mobilePhone', this.state.mobilePhone);

                formData.append('email', this.state.email);

                console.log(formData)
                let options = {
                    method: 'PUT',
                    body: formData,
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'multipart/form-data',
                    },
                };

                return fetch(apiUrl, options)
                    .then((response) => response.json())
                    .then((data) => {
                        Alert.alert(
                            'Pesan',
                            'Update Berhasil',
                            [
                                //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                                //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                                {
                                    text: 'OK', onPress: () => {
                                        this.setState({
                                            visible: false
                                        });
                                        this.props.navigation.navigate("Profil")
                                    }

                                },
                            ],
                            { cancelable: false }
                        )
                    })
                    .catch((error) => {
                        console.log(error);
                    })

            })




            //     return fetch("http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile/" + this.state.userId, {
            //         method: 'PUT',
            //         headers: {
            //             'Accept': 'application/json',
            //             'Content-Type': 'application/json',
            //             'Authorization': 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ=='
            //         },
            //         body: JSON.stringify({
            //             profilePicture: ";base64," + this.state.encodedData,
            //             mobilePhone: this.state.mobilePhone,
            //             email: this.state.email,
            //         })
            //     })
            //         .then(response => response.json())
            //         .then(
            //         Alert.alert(
            //             'Pesan',
            //             'Update Berhasil',
            //             [
            //                 //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
            //                 //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
            //                 {
            //                     text: 'OK', onPress: () => {
            //                         this.props.navigation.navigate("Profil");
            //                     }
            //                 },
            //             ],
            //             { cancelable: false }
            //         )
            //         )
            // }
        }
    }
}



